use eval;

macro_rules! lambda_test {
    (($($lambda_body:tt)*) -> $expected_result:expr) => ({
        let lambda_struct = lambda_expr!($($lambda_body)*);
        let mut res = format!("{:} -> ", lambda_struct).to_owned();
        let result = eval::interpret(lambda_struct).unwrap();
        res.push_str(format!("{:}", result).as_ref());
        assert_eq!(res, String::from($expected_result));
    })
}

#[test] fn test_lambda_expr() {
    lambda_test!(
        (bino 2 3 4 10) ->
        "bino 2 3 4 10 -> 234"
    );
    lambda_test!(
        ((λ x.λ y.sub y x) 10 20) ->
        "(λ x.λ y.sub y x) 10 20 -> 10"
    );
    lambda_test!(
        ((λ f.λ y.λ x.f y x) (λ x.λ y.y) 3 4) ->
        "(λ f.λ y.λ x.f y x) (λ x.λ y.y) 3 4 -> 4"
    );
    lambda_test!(
        ((λ x.x 2 3) (if (gt 3 1) add gt)) ->
        "(λ x.x 2 3) (if (gt 3 1) add gt) -> 5"
    );
    lambda_test!(
        (eq ((λ a.λ b.λ c.λ x.add (add (mul (mul x x) a) (mul x b)) c) 2 3 4 10) (bino 2 3 4 10)) ->
        "eq ((λ a.λ b.λ c.λ x.add (add (mul (mul x x) a) (mul x b)) c) 2 3 4 10) (bino 2 3 4 10) -> true"
    );
}

#[test] fn test_ycomb() {
    lambda_test!(
        ((λ f.(λ x.f (x x)) (λ x.f (x x))) (λ fac.λ n.if (eq n 0) 1 (mul n (fac (sub n 1)))) 10) ->
        "(λ f.(λ x.f (x x)) (λ x.f (x x))) (λ fac.λ n.if (eq n 0) 1 (mul n (fac (sub n 1)))) 10 -> 3628800"
    );
}

#[test] fn test_lazy_or() {
    lambda_test!(
        (or true ((λ x.x x) (λ x.x x))) ->
        "or true ((λ x.x x) (λ x.x x)) -> true"
    );
}
