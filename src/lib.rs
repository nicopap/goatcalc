//! An implementation of the λ-calculus
#![feature(try_from,box_patterns,box_syntax,type_ascription)]

#[cfg(test)] #[macro_use] extern crate  pretty_assertions;
#[macro_use] extern crate quick_error;
pub extern crate polytype;
extern crate fxhash;

#[macro_use] pub mod lambda;
pub mod eval;
pub mod richlambda;
#[macro_use] pub mod typed;
#[cfg(test)] mod test;

pub use lambda::Expression;
pub use eval::interpret;
