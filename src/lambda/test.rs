use super::conversion::{beta_reduce,eta_reduce,Error};
use super::*;

macro_rules! reduction_test {
    (doesnot $reduction:ident ($($unreduced:tt)*) because $error:path) => ({
        let lambda_struct : Expression<String,String> = lambda_expr!($($unreduced)*);
        let result = $reduction(lambda_struct).unwrap_err();
        assert_eq!(result, $error);
    });
    ($reduction:ident ($($unreduced:tt)*) becomes $reduced:tt) => ({
        let lambda_struct : Expression<String,String> = lambda_expr!($($unreduced)*);
        let result = $reduction(lambda_struct).map(|res| format!("{:}", res));
        assert_eq!(result, Ok($reduced.to_owned()));
    })
}

#[test]
fn test_reductions_simple() {
    reduction_test!(
        eta_reduce (λ x.f x)
        becomes "f"
    );
    reduction_test!(
        beta_reduce ((λ x.f x) y)
        becomes "f y"
    );
}

#[test]
fn eta_avoid_free() {
    reduction_test!(
        doesnot eta_reduce (λ x. (λ y. f (y x)) x)
        because Error::EtaReductionXfreeInF
    );
    reduction_test!(
        doesnot eta_reduce (λ x. (f x x) x)
        because Error::EtaReductionXfreeInF
    );
    reduction_test!(
        doesnot eta_reduce (λ x. "f" x)
        because Error::EtaReductionNotFunction
    );
}

#[test]
fn fancy_eta() {
    reduction_test!(
        eta_reduce (λ x.(λ y.f y (λ z.(λ x.f x) y z)) x)
        becomes "λ y.f y (λ z.(λ x.f x) y z)"
    );
}

#[test]
fn test_ycomb() {
    reduction_test!(
        beta_reduce ( (λ f.(λ x.f (x x)) (λ x.f (x x))) (λ x.x) )
        becomes "(λ x.(λ x.x) (x x)) (λ x.(λ x.x) (x x))"
    );
    reduction_test!(
        beta_reduce ( (λ x.(λ x.x) (x x)) (λ x.(λ x.x) (x x)) )
        becomes "(λ x.x) ((λ x.(λ x.x) (x x)) (λ x.(λ x.x) (x x)))"
    );
    reduction_test!(
        beta_reduce ( (λ x.x) ((λ x.(λ x.x) (x x)) (λ x.(λ x.x) (x x))) )
        becomes "(λ x.(λ x.x) (x x)) (λ x.(λ x.x) (x x))"
    );
}
