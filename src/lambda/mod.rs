//! Defines the lambda calculus as an expression. The implementation is
//! generic to accept any "literal" and does not assume any evaluation
//! scheme
pub mod conversion;
#[macro_use] pub mod expression;
pub use self::expression::Expression;

#[cfg(test)] mod test;
