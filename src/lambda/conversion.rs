//! Implements various λ-calculus reduction rules to λ-expressions
use super::Expression;

quick_error! {
    #[derive(Debug,PartialEq,Eq)]
    pub enum Error {
        EtaReductionOnNonLambda {
            description("invalid η-reduction: expression should be `λx.(f x)` \
                        but the outer expression is not a λ-abstraction")
        }
        EtaReductionWithNoApplication {
            description("invalid η-reduction: expression should be \
                        `λx.(f x)` but the inner expression is not \
                        an application.")
        }
        EtaReductionDifferentIdent {
            description("invalid η-reduction: expression should be `λx.(f x)` \
                        but the inner argument is not x \
                        (it is `λx.(f E)` where `E != x`)")
        }
        EtaReductionXfreeInF {
            description("invalid η-reduction: expression is `λx.(f x)` but \
                        `x` appears free in `f`")
        }
        EtaReductionNotFunction {
            description("invalid η-reduction: expression is `λx.(f x)` but \
                        `f` is a literal (`f` must be a function)")
        }
        BetaReductionOnNonAppli {
            description("invalid β-reduction: expression should be `(λx.M)E` \
                        but the outer expression is not an application")
        }
        BetaReductionWithNoLambda {
            description("invalid β-reduction: expression should be `(λx.M)E` \
                        but the inner expression is not a λ-abstraction")
        }
    }
}

type Result<R> = ::std::result::Result<R, Error>;

fn replace<N,L>(
    body: Expression<N,L>,
    to_replace: &N,
    replacement: Expression<N,L>
) -> Expression<N,L>
    where N: PartialEq + Clone,
          L: Clone,
{
    use Expression::*;
    match body {
        Application {function, argument} =>
            Application {
                function: box replace(*function, to_replace, replacement.clone()),
                argument: box replace(*argument, to_replace, replacement)
            },

        Lambda {body, binding} =>
            if binding == *to_replace {
                Lambda {body, binding}
            } else {
                Lambda {
                    body: box replace(*body, to_replace, replacement),
                    binding: binding,
                }
            },
        Ident(ref name) if name==to_replace => replacement,
        ident@Ident(_) /*otherwise*/        => ident,
        lit@Literal(_)                      => lit,
    }
}

fn appear_free_in<N:PartialEq, L>(var: &N, expr: &Expression<N,L>) -> bool {
    use Expression::*;
    match *expr {
        Application {ref function, ref argument} =>
            appear_free_in(var, function) || appear_free_in(var, argument),

        //Never will be free, is bound :(
        Lambda{ref binding,..} if binding==var => false,
        Lambda{ref body, ..} /*otherwise*/     => appear_free_in(var, body),
        Ident(ref name) if name == var         => true,
        Ident(_) | Literal(_)                  => false,
    }
}

pub fn beta_reduce<N,L>(expr: Expression<N,L>) -> Result<Expression<N,L>>
    where N: PartialEq + Clone,
          L: Clone,
{
    use Expression::*;
    if let Application {function, argument} = expr {
        if let Lambda {ref body, ref binding} = *function {
            let body_expr = body.clone();
            Ok(replace(*body_expr, binding, *argument))
        } else { Err(Error::BetaReductionWithNoLambda) }
    } else { Err(Error::BetaReductionOnNonAppli) }
}

pub fn eta_reduce<N,L>(expr: Expression<N,L>) -> Result<Expression<N,L>>
    where N: PartialEq + Clone,
          L: Clone,
{
    use Expression::*;
    // How do I change that to something smooth?
    if let Lambda {body, binding} = expr {
        if let Application {ref function, ref argument} = *body {
            if let Ident(ref name) = **argument {
                if name == &binding {
                    if let Literal(_) = **function {
                        Err(Error::EtaReductionNotFunction)
                    } else if !appear_free_in(&binding, function) {
                            Ok(*(function.clone()))
                    } else { Err(Error::EtaReductionXfreeInF) }
                } else { Err(Error::EtaReductionDifferentIdent) }
            } else { Err(Error::EtaReductionDifferentIdent) }
        } else { Err(Error::EtaReductionWithNoApplication) }
    } else { Err(Error::EtaReductionOnNonLambda) }
}
