//! Defines a λ-expression data structure and a macro to write that
//! data structure in a natural manner.


use std::fmt::{self,Display};

/// An expression in the λ-calculus
/// Generic over the literal `Lit` choosen by the implementer.
#[derive(Clone,Debug)]
pub enum Expression<Name,Lit> {
    /// A λ-expression (`λx. Expr`)
    Lambda {
        /// The name of the binding (it is `x` in `λx. Expr`)
        binding: Name,
        /// The body (it is `Expr` in `λx. Expr`)
        body: Box<Expression<Name,Lit>>
    },
    /// An application (such as `f x`). A function only accepts a single input,
    /// multiple inputs are emulated with currying (function with argument
    /// returns another function that accepts an argument)
    Application {
        /// `f` in `f x`
        function: Box<Expression<Name,Lit>>,
        /// `x` in `f x`
        argument: Box<Expression<Name,Lit>>
    },
    /// A literal.
    Literal(Lit),
    /// A free or bound variable.
    Ident(Name),
}

/// A macro to write lambda expressions naturally.
///
/// one would use it that way:
/// `lambda_expr!((λ x.x 2 3) (if (gt 3 1) add gt))`
///
/// # Notes
///
/// The λ should be separated from other names.  Furthermore, only identifiers
/// in the sense of Rust are accepted as variable names.
///
/// This macro is heavily relient on `From<X>` traits to convert
/// constants into data, so make sure to implement `From<X>` for your `Lit`
/// generic in `Expression`.
#[macro_export]
macro_rules! lambda_expr {
    (λ $binding:ident . $($body:tt)+) => (
        $crate::lambda::Expression::Lambda {
            binding: stringify!($binding).to_owned(),
            body: Box::new(lambda_expr!($($body)+)),
        }
    );
    (($($rem:tt)+)) => ( lambda_expr!($($rem)+) );
    (true)  => ( $crate::Expression::from(true) );
    (false) => ( $crate::Expression::from(false));
    ($name:ident) => ( $crate::Expression::Ident(stringify!($name).to_owned()) );
    ($lite:tt ) => ( $crate::Expression::from($lite) );

    // Two next rules magically enables left associativity by inserting
    // parenthesis between any two contiguous token.
    ($lval:tt $rval:tt $($rem:tt)+) => (
        lambda_expr!(($lval $rval) $($rem)+)
    );
    ($lval:tt $rval:tt) => (
        $crate::Expression::Application {
            function: Box::new(lambda_expr!($lval)),
            argument: Box::new(lambda_expr!($rval)),
        }
    );
}

impl<N:Display, L:Display> fmt::Display for Expression<N,L> {
    /// Represents an expression. The format is the same as the one accepted
    /// by the lambda_expr! macro.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Expression::*;
        match *self {
            Lambda { ref binding, ref body } =>
                write!(f, "λ {:}.{:}", binding, body),

            Application { ref function, ref argument } => {
                match **function {
                    Lambda {..} =>
                        write!(f, "({:}) ", function),
                    Application {..} | Literal(_) | Ident(_) =>
                        write!(f, "{:} ", function),
                }?;
                match **argument {
                    Lambda {..} | Application {..} =>
                        write!(f, "({:})", argument),
                    Literal(_) | Ident(_) =>
                        write!(f, "{:}", argument),
                }
            },
            Literal(ref literal) =>
                write!(f, "{:}", literal),
            Ident( ref name) =>
                write!(f, "{:}", name),
        }
    }
}

impl<N,T:Copy,L:From<T>> From<T> for Expression<N,L> {
    fn from(x: T) -> Expression<N,L> {
        Expression::Literal(x.into())
    }
}

