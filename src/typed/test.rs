use std::collections::HashMap;
use std::convert::{TryFrom,TryInto};

use fxhash::FxHashMap;

use super::*;
use ::richlambda::Expression;
use ::lambda;
use polytype::{TypeSchema, Type,Name};


macro_rules! context {
    {$($x:ident : $sig:expr),*} => ({
        let mut ret = FxHashMap::default();
        $( ret.insert(stringify!($x).to_owned(), $sig); )*
        ret
    })
}

#[derive(Clone, Eq, PartialEq,Debug)]
enum CustomType { Arrow, Int, Bool, List, Model }

impl Name for CustomType {
    fn arrow() -> CustomType { CustomType::Arrow }
    fn show(&self) -> String {
        use self::CustomType::*;
        match self {
            Arrow => "->", Int => "Int", Bool => "Bool",
            List => "List", Model => "Model",
        }.to_owned()
    }
    fn parse(string: &str) -> Result<CustomType,()> {
        use self::CustomType::*;
        match string {
            "->" => Ok(Arrow), "Int" => Ok(Int), "Bool" => Ok(Bool),
            "List" => Ok(List), "Model" => Ok(Model), _ => Err(()),
        }
    }
}


impl Typeable<CustomType> for i64 {
    fn type_(&self) -> Type<CustomType> {
        Type::Constructed(CustomType::Int,vec![])
    }
}

impl TryFrom<&'static str> for CustomType {
    type Error=();
    fn try_from(string: &str) -> Result<CustomType,()> {
        CustomType::parse(string)
    }
}

macro_rules! inference_test {
    (
        lambda : $type:ty {$($lambda_body:tt)*}
        context {$($context_body:tt)*}
        expects {$expects:expr}
    ) => ({
        let simple_lambda : $type = lambda_expr!($($lambda_body)*);
        let rich_lambda : Expression<_,_,_> = simple_lambda.into();
        let types : HashMap<_,TypeSchema<CustomType>,_> =
            context!{$($context_body)*};
        let typed_lambda = judge(rich_lambda, &types);
        assert_eq!(
            format!("{}", typed_lambda.unwrap().annot()),
            $expects.to_owned(),
        );
    })
}

#[test]
fn test_bino_inference() {
    inference_test!(
        lambda : lambda::Expression<String,i64> {
            λ bino. λ k.λ l.
                eq
                    ((λ a.λ b.λ c.λ x.
                        add (add (mul (mul x x) a) (mul x b)) c
                     ) 2 l 4 k
                    )
                    (bino 2 k 4 l)
        }
        context {
            eq: type_expr!(@a. a -> a -> Bool),
            add: type_expr!(Int -> Int -> Int),
            mul: type_expr!(Int -> Int -> Int)
        }
        expects { "(Int → Int → Int → Int → Int) → Int → Int → Bool" }
    );
    inference_test!(
        lambda : lambda::Expression<String,i64> {
            λ bino. λ k.λ l.
                eq
                    ((λ a.λ b.λ c.λ x.
                        add (add (mul (mul x x) a) (mul x b)) c
                     ) 2 1 4 3
                    )
                    (bino 2 k 4 l)
        }
        context {
            eq: type_expr!(@a. a -> a -> Bool),
            add: type_expr!(Int -> Int -> Int),
            mul: type_expr!(Int -> Int -> Int)
        }
        expects { "(Int → t1 → Int → t2 → Int) → t1 → t2 → Bool" }
    )
}

#[test]
fn test_fac_inference() {
    inference_test!(
        lambda : lambda::Expression<String,i64> {
            λ fac.λ n.if (eq n 0) 1 (mul n (fac (sub n 1)))
        }
        context {
            eq: type_expr!(Int -> Int -> Bool),
            sub: type_expr!(Int -> Int -> Int),
            mul: type_expr!(Int -> Int -> Int),
            if: type_expr!(@a. Bool -> a -> a -> a)
        }
        expects { "(Int → Int) → Int → Int" }
    )
}

#[test]
fn test_ident_dup() {
    inference_test!(
        lambda : lambda::Expression<String,i64> {
            λ fac.λ n.if ((λ x.x) (eq n 0)) 1 (mul n (fac ((λ x.x) (sub n 1))))
        }
        context {
            eq: type_expr!(Int -> Int -> Bool),
            sub: type_expr!(Int -> Int -> Int),
            mul: type_expr!(Int -> Int -> Int),
            if: type_expr!(@a. Bool -> a -> a -> a)
        }
        expects { "(Int → Int) → Int → Int" }
    )
}


// ```
// f colormsg color =
//     div [] [onInput (colormsg color)]
// ```
// turns into:
// ```
// let f =
//     λ colormsg . λ color . div NIL (Cons (onInput (colormsg color)) NIL)
// ```
#[test]
fn test_experimental() {
    inference_test!(
        lambda : lambda::Expression<String,i64> {
            (λ colormsg . λ color .
                div NIL (Cons (onInput (colormsg color)) NIL)
            ) unkown Model
        }
        context {
            div: type_expr!(@a.@b. List a -> List b -> b),
            NIL: type_expr!(@a. List a),
            Cons: type_expr!(@a. a -> List a -> List a),
            onInput: type_expr!(@a. a -> a),
            unkown: type_expr!(@a. a -> a),
            Model: type_expr!(Model)
        }
        expects { "Model" }
    )
}

#[test]
fn test_experimental_unapplied() {
    inference_test!(
        lambda : lambda::Expression<String,i64> {
            (λ colormsg . λ color .
                div NIL (Cons (onInput (colormsg color)) NIL)
            )
        }
        context {
            div: type_expr!(@a.@b. List a -> List b -> b),
            NIL: type_expr!(@a. List a),
            Cons: type_expr!(@a. a -> List a -> List a),
            onInput: type_expr!(@a. a -> a)
        }
        expects { "(t1 → t13) → t1 → t13" }
    )
}
#[test]
fn test_simple() {
    inference_test!(
        lambda : lambda::Expression<String,i64> { (λ h . λ t . Cons h t) }
        context { Cons: type_expr!(@a. a -> List a -> List a) }
        expects { "t0 → List(t0) → List(t0)" }
    );
    inference_test!(
        lambda : lambda::Expression<String,i64> {
            (λ h . λ t . Cons ((λ x. x) h) t)
        }
        context { Cons: type_expr!(@a. a -> List a -> List a) }
        expects { "t4 → List(t4) → List(t4)" }
    )
}
