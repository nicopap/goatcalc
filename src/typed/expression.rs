/// Creates a `polytype::TypeSchema` type based on a familiar grammar.
///
/// the `polytype` crate exposes types to manipulate type inferences,
/// it also exposes macros to define those types without too much hassle.
/// However, it was not enough to me. I implemented a DSL that translates to
/// those types.
///
/// In summary, `type_expr!` creates arbitrary `polytype::TypeSchema`.
/// refer to the wikipedia article on the Hindley-Milner type system for
/// more details:
/// <https://en.wikipedia.org/wiki/Hindley%E2%80%93Milner_type_system#Syntax>
///
/// The syntax accepts quantifier bindings at the beginning under the form of
/// `@a. @b. ... @n. <monotype>` where each `a..n` have independent LOWERCASE
/// names. Those are the type variables that will be present in the monotype.
/// The `@` stands for `∀`.
///
/// A monotype has the following syntax:
/// ```non-rust
/// <monotype> =
///   <monotype> -> <monotype>
///   Name <monotype>*
///   lowercase
///   (<monotype>)
/// ```
///
/// With some limitations (ambiguous cases and maybe a bit more).
///
/// The `Name`'s are uppercase "terminal" type or type constructors. They will
/// be converted into the type you want using the `&str::TryInto<type>` trait.
/// The result will be unwrapped as following:
/// `stringify!(Name).try_into().unwrap()`. So make sure that you handle all
/// the symbols you use.
///
/// The `lowercase` are free variables. They should be bound in the polytype
/// section or panic may ensure.
///
/// # Example
///
/// ```
/// #![feature(try_from)]
/// #[macro_use] extern crate goatcalc;
/// use std::convert::{TryInto,TryFrom};
/// use goatcalc::polytype;
/// #[derive(PartialEq, Eq, Clone)]
/// enum Tn { Arrow, List, Map, Bool, Maybe, Int }
///
/// fn parse_tn(string: &str) -> Result<Tn,()> {
///     use self::Tn::*;
///     match string {
///         "->"    => Ok(Arrow), "List" => Ok(List),
///         "Map"   => Ok(Map),   "Bool" => Ok(Bool),
///         "Maybe" => Ok(Maybe), "Int"  => Ok(Int),
///         _ => Err(()),
///     }
/// }
/// impl TryFrom<&'static str> for Tn {
///     type Error=();
///     fn try_from(string: &str) -> Result<Tn,()> { parse_tn(string) }
/// }
/// impl polytype::Name for Tn {
///     fn arrow() -> Self { Tn::Arrow }
///     fn show(&self) -> String {
///         use self::Tn::*;
///         match *self {
///             Arrow => "->",   List => "List", Map   => "Map",
///             Bool  => "Bool", Int  => "Int",  Maybe => "Maybe",
///         }.to_owned()
///     }
///     fn parse(string: &str) -> Result<Tn,()> { parse_tn(string) }
/// }
///
/// // ...
///
/// # fn main() {
/// let type_schema : polytype::TypeSchema<Tn> =
///     type_expr!(@a.@b.(a -> b) -> List a -> List b);
///     // where Tn : TryFrom<&str> + polytype::Name
/// assert_eq!(
///     format!("{}", type_schema),
///     "∀t0. ∀t1. (t0 → t1) → List(t0) → List(t1)".to_owned()
/// );
///
/// //A more contrieved example: you can get as fancy as you want
/// let crazy_type : polytype::TypeSchema<Tn> = type_expr!(
///     @t0. @t1. @t2. @t3.
///         (t0 -> t1 -> t2 -> Maybe t3)
///         -> (Map (List t0) (t1 -> t2) -> Map t0 Bool)
///         -> (Map t0 t2 -> Map t0 t3)
///         -> List Int
/// );
/// assert_eq!(
///     format!("{}", crazy_type),
///     "∀t0. ∀t1. ∀t2. ∀t3. \
///         (t0 → t1 → t2 → Maybe(t3)) \
///         → (Map(List(t0),t1 → t2) → Map(t0,Bool)) \
///         → (Map(t0,t2) → Map(t0,t3)) \
///         → List(Int)".to_owned(),
/// );
/// # }
/// ```
///
/// # Panics
///
/// * When the provided terminal `Name`'s cannot be converted into the `Type`
///   type
/// * When there is a free type variable (a `lowercase` that didn't get bound
///   in a `@lowercase.` quantification)
/// * Probably when the same variable gets bound multiple times.
///
/// # Errors
///
/// The compilation errors might be confusing. Here are where you
/// should look first when you run into an issue:
///
/// * Make sure that your macro invocation complies with the grammar
///   provided here.
/// * Make sure you implemented `polytype::Name` and `TryFrom<&'static str>`
///   for your type.
#[macro_export]
macro_rules! type_expr {
    // polytype constructions (starts with `where`)
    (where $variable_names:expr ; @ $binding:ident . $($body:tt)+) => ({
        let index = $variable_names.len() as u16;
        $variable_names.insert(stringify!($binding), index);
        $crate::polytype::TypeSchema::Polytype {
            variable: index,
            body: Box::new(type_expr!(where $variable_names ; $($body)+)),
        }
    });
    (where $vn:expr ; $($rem:tt)+) => (
        $crate::polytype::TypeSchema::Monotype(
            type_expr!(in $vn; {} $($rem)+)
        )
    );

    // Monotypes construction (starts with `in $vn;`)

    // Arrow case
    (in $vn:expr ; {$($args:tt)+} -> $($rem:tt)+) => (
        type_expr!(in $vn ; [[ { @ {$($args)+} { type_expr!(in $vn; {} $($rem)+) } } ]])
    );
    // first Name case
    (in $vn:expr ; {} $name:ident $($rem:tt)*) => (
        type_expr!(in $vn; {$name} $($rem)*)
    );
    // following Name case
    (in $vn:expr ; {$($pres:tt)+} $name:ident $($rem:tt)*) => (
        type_expr!(in $vn; { $($pres)+ {$name} } $($rem)*)
    );
    // parenthesis
    (in $vn:expr ; {$($pres:tt)*} ($($parent:tt)+) $($rem:tt)*) => (
        type_expr!(in $vn;
            { $($pres)* { type_expr!(in $vn; {} $($parent)+) } }
            $($rem)*
        )
    );

    // Terminal
    (in $vn:expr ; {$($pres:tt)+} ) => (
        type_expr!(in $vn ; [[ { $($pres)+ } ]])
    );
    // Builds the final type
    (in $vn:expr ; [[ { $name:ident $({$($args:tt)+})* } ]]) => ({
        $vn.get(stringify!($name)).map(|&x| $crate::polytype::Type::Variable(x))
            .unwrap_or_else(|| {
                $crate::polytype::Type::Constructed(
                    stringify!($name).try_into().unwrap(),
                    vec![$( type_expr!(in $vn ; [[ { $($args)+ } ]])),*]
                )
        })
    });
    (in $vn:expr ; [[ { @ $({$($args:tt)+})* } ]]) => (
        $crate::polytype::Type::arrow(
            $( type_expr!(in $vn ; [[ { $($args)+ } ]])),*
        )
    );
    // We used `type_expr!( .. type_expr!(..) .. )` it is possible the inner
    // macros didn't get executed, so we expand them so as to make sure they
    // will be ran, and expand to their proper values
    (in $vn:expr ; [[ { $($args:tt)* } ]]) => (
        $($args)*
    );
    // User interface
    ($($rem:tt)+) => ({
        let mut names : ::std::collections::HashMap<&'static str, u16> =
            ::std::collections::HashMap::new();
        type_expr!(where names ; $($rem)+)
    });
}

