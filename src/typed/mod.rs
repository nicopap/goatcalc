//! Hindley-Milner type inference for the "rich" λ-calculus.
//!
//! WARNING: if the provided expression doesn't use unique identifiers for
//! each name bindings, the `judge` function might panic.
#[macro_use] mod expression;
#[cfg(test)] mod test;

use std::hash::Hash;
use std::fmt::{Debug,Display};

use fxhash::FxHashMap;
use polytype::{self, Context, Type, UnificationError};

use richlambda::{Expression, Expression_};
use richlambda::expression::Anchored;

quick_error! {
    /// A typing error.
    #[derive(Debug)]
    pub enum TypeError {
        /// Attempt to type check a pattern, not yet implemented
        ComplicatedPattern {
            description("Attempt to type check a pattern, not yet implemented")
        }
        /// A conflict in type has been detected
        TypeUnification(expression: String, type1 : String, type2 : String) {
            display("The type of the expression `{}` couldn't be deduced: \
            it is either `{}` or `{}`.",expression, type1, type2)
        }
        /// A generic error related to unification
        VarUnification {
            description("Some bad type variables were used")
        }
        /// Attempt to type check the free variable
        FreeVar(var_name: String) {
            display("Attempt to type check the free variable: {}", var_name)
        }
    }
}

/// A literal in a rich λ-expression can have its type inferred.
pub trait Typeable<Tn> where Tn: polytype::Name {
    /// The type of the given
    fn type_(&self) -> polytype::Type<Tn>;
}

/// The rich λ-calculus augmented with a typing system
pub type TypedLambda<Tn,In, L> = Expression<In,L,polytype::Type<Tn>>;

/// The mapping of variables to their inferred type
pub type Assumptions<IdN,TyN> = FxHashMap<IdN,polytype::TypeSchema<TyN>>;

fn judge_internal<Tn,In,L>(
    expression: Expression<In,L,()>,
    assumptions: &Assumptions<In,Tn>,
    dyn_assum: &mut FxHashMap<In,Type<Tn>>,
    context: &mut Context<Tn>,
) ->
    Result<TypedLambda<Tn,In,L>,TypeError>
    where L: Typeable<Tn> + Clone + Display,
          Tn: polytype::Name + Debug,
          In: Eq + Hash + Clone + Display + Debug,
{
    macro_rules! judge_sub {
        ($sub_expr:expr) => (
            judge_internal($sub_expr, assumptions, dyn_assum, context)?
        )
    };
    use self::Expression_::*;
    let expression_show = format!("{}", expression);
    match expression.inner_own() {
        Lambda { binding, box body } => {
            let name = binding.clone();
            dyn_assum.insert(name.clone(), context.new_variable());
            let Anchored(body_type, body) = judge_internal(
                body, assumptions, dyn_assum, context
            )?;
            let binding_var = dyn_assum.remove(&name).expect(
                "a name shouldn't be removed from context before \
                leaving the scope in which it was defined."
            );
            Ok(Anchored(
                Type::arrow(binding_var, body_type.clone()),
                Lambda { binding, body: box Anchored(body_type,body) },
            ))
        },

        Application { box function, box argument } => {
            let Anchored(fun_type, fun) = judge_sub!(function);
            let Anchored(arg_type, arg) = judge_sub!(argument);
            let arg_var = context.new_variable();
            context.unify(
                &fun_type,
                &Type::arrow(arg_type.clone(), arg_var.clone()),
            ).map_err(|err|
                if let UnificationError::Failure(type1,type2) = err {
                    TypeError::TypeUnification(
                        format!("{}", expression_show),
                        format!("{}", type1),
                        format!("{}", type2),
                    )
                } else { TypeError::VarUnification }
            )?;
            let deduced_type = arg_var.apply(context);
            dyn_assum.values_mut().for_each(|type_| type_.apply_mut(context));
            Ok(Anchored(
                deduced_type,
                Application {
                    function: box Anchored(fun_type.apply(context), fun),
                    argument: box Anchored(arg_type.apply(context), arg),
                }
            ))
        },

        Literal(literal) => Ok(Anchored(
            literal.type_(),
            Literal(literal),
        )),

        Ident(name) => {
            let inst_type = assumptions
                .get(&name)
                .map(|poly_type| poly_type.instantiate(context))
                .or_else(|| dyn_assum.get(&name).cloned())
                .ok_or_else(|| TypeError::FreeVar(format!("{:?}", name)) )?;
            Ok(Anchored( inst_type, Ident(name) ))
        },

        Let {..} => {
            unimplemented!()
        },

        Letrec {..} => {
            unimplemented!()
        },

        Case {..} => {
            unimplemented!()
        },
    }
}

/// Marks an untyped λ-expression with its type `polytype::Type`
///
/// Based on given `assumptions` (the type of free variables in the
/// expression) and the types of the literals. One can determine the
/// type of the whole expression and all its nodes.
///
/// # Errors
///
/// When there is a type error, or there is free variables that are not
/// present in the provided `assumptions` list.
///
/// # Panics
///
/// When in the expression, unique identifiers are not used for each
/// bindings. This may fail silently as well.
pub fn judge<Tn,In,L>(
    expression: Expression<In,L,()>,
    assumptions: &Assumptions<In,Tn>,
) ->
    Result<TypedLambda<Tn,In,L>,TypeError>
    where L: Typeable<Tn> + Clone + Display,
          Tn: polytype::Name + Debug,
          In: Eq + Hash + Clone + Display + Debug,
{
    let mut context = Context::default();
    let mut dyn_assum = FxHashMap::default();
    judge_internal(expression, assumptions, &mut dyn_assum, &mut context)
}
