use std::convert::{TryFrom,TryInto};
use std::collections::VecDeque;
use std::fmt;
use std::rc::Rc;

use fxhash::FxHashMap;

use lambda::Expression;

impl fmt::Debug for IntermediateValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::IntermediateValue::*; match *self {
            NativeFunction {ref arguments, ref argument_count, name, ..} =>
                write!(f, "{}{{clos:{:?}, with {:} args}}",
                       name, arguments, argument_count),
            Thunk {ref context, ref expression} =>
                write!(f, "({})[{:?}]", expression, context),
            Evaluated(ref literal) => write!(f, "Val({:?})", literal),
        }
    }
}
impl fmt::Display for IntermediateValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::IntermediateValue::*; match *self {
            NativeFunction {name, ..} => write!(f, "{}", name),
            Evaluated(ref literal)        => write!(f, "{}", literal),
            Thunk {ref expression, ..}    => write!(f, "{}", expression),
        }
    }
}

impl From<i64>  for IntermediateValue {fn from(x: i64 ) -> IntermediateValue {IntermediateValue::Evaluated(Value::Int(x) )}}
impl From<bool> for IntermediateValue {fn from(x: bool) -> IntermediateValue {IntermediateValue::Evaluated(Value::Bool(x))}}
impl From<Value> for IntermediateValue {fn from(x: Value) -> IntermediateValue { IntermediateValue::Evaluated(x) }}
impl From<i64>  for Value {fn from(x: i64 ) -> Value {Value::Int(x) }}
impl From<bool> for Value {fn from(x: bool) -> Value {Value::Bool(x)}}
impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Value::Bool(b) => write!(f, "{}", b),
            Value::Int(i)  => write!(f, "{}", i),
        }
    }
}
macro_rules! impl_try_from_intermediate_value { ($t:ty ;into $alt:path) => (
    impl TryFrom<IntermediateValue> for $t {
        type Error = String;
        fn try_from(v: IntermediateValue) -> Result<$t,String> {
            v.strict().and_then(|x| match x {
                $alt(val) => Ok(val),
                anyelse => Err(format!("type error expected {}, got {:?}",
                                stringify!($t), anyelse)),
            })
        }
    }
) }
impl_try_from_intermediate_value!(i64 ;into Value::Int);
impl_try_from_intermediate_value!(bool ;into Value::Bool);
impl TryFrom<IntermediateValue> for Value {
    type Error = String;
    fn try_from(v: IntermediateValue) -> Result<Value,String> {
        match v {
            IntermediateValue::Evaluated(value) => Ok(value),
            anyelse =>
                Err(format!("attempted to convert an unevaluated expression \
                            ({}) into a value", anyelse)),
        }
    }
}


/// Evaluates a λ-expression using the given context.
fn evaluate(expression: Expression<String,Value>, context: Rc<Context>)
    -> Result<IntermediateValue,String>
{
    use Expression::*;
    use self::IntermediateValue::Thunk;
    match expression {
        expression@Lambda { .. } =>
            Ok(Thunk { context, expression }),
        Application {box function, box argument} => {
            let function_value = evaluate(function, context.clone())?;
            let argument_value = evaluate(argument, context)?;
            function_value.apply(argument_value)
        },
        Ident(ref name) =>
            match context.get(name) {
                None => Err(format!("free variable {} not in context",name)),
                Some(val) => Ok((*val).clone()),
            },
        Literal(value) =>
            Ok(value.into()),
    }
}


type Context = FxHashMap<String,IntermediateValue>;

/// A literal. Can either be a boolean or an integer
#[derive(Clone, Debug)]
pub enum Value {
    Int(i64),
    Bool(bool),
}


#[derive(Clone)]
pub enum IntermediateValue {
    Evaluated(Value),
    NativeFunction {
        argument_count: u8,
        arguments: VecDeque<IntermediateValue>,
        function: fn(VecDeque<IntermediateValue>) -> IntermediateValue,
        name: &'static str,
    },
    Thunk {
        context: Rc<Context>,
        expression: Expression<String,Value>,
    },
}

impl IntermediateValue {
    fn apply(self, argument: IntermediateValue)
        -> Result<IntermediateValue,String>
    {
        use self::IntermediateValue::*;
        use self::Expression::*;
        match self {
            NativeFunction {mut arguments, function, argument_count, name} => {
                arguments.push_back(argument);
                Ok(if arguments.len() == (argument_count as usize) {
                    (function)(arguments)
                } else {
                    NativeFunction {arguments, function, argument_count, name}
                })
            },
            Thunk { context, expression } =>
                match expression {
                    Lambda {box body, binding} => {
                        let mut new_context = Context::clone(&context);
                        new_context.insert(binding, argument);
                        Ok(Thunk {
                            context: Rc::new(new_context),
                            expression: body,
                        })
                    },
                    anyelse => {
                        let function_value = evaluate(anyelse, context)?;
                        function_value.apply(argument)
                    },
                },
            anyelse =>
                Err(format!("attempt to apply {} to {} \
                            (which is not a function)", argument, anyelse)),
        }
    }

    fn strict(self) -> Result<Value,String> {
        use self::IntermediateValue::*;
        match self {
            Thunk {expression, context} =>
                evaluate(expression, context)?.strict(),
            NativeFunction {..} =>
                Err("a native function with missing arguments \
                            doesn't have a strict form".to_owned()),
            Evaluated(value) =>
                Ok(value),
        }
    }
}

/// Evaluates a lambda expression and returns the string representation of
/// the resulting value.
pub fn interpret(expression: Expression<String,Value>) -> Result<Value,String> {
    macro_rules! closure {
        {fn $name:ident ($($args:ident : $tys:ty),+) -> $retty:ty $body:block} => ({
            fn function(mut arg_stack: VecDeque<IntermediateValue>)
                -> IntermediateValue {
                $(let $args : $tys
                    = arg_stack.pop_front().unwrap().try_into().unwrap();
                )+
                IntermediateValue::from($body)
            };
            IntermediateValue::NativeFunction {
                function: function,
                argument_count: [$(stringify!($args)),+].len() as u8,
                arguments: VecDeque::new(),
                name: stringify!($name),
            }
        })
    }
    macro_rules! make_context { {$($key:expr => $value:expr,)*} => ({
            let mut fresh_context = Context::default();
            $(fresh_context.insert($key.into(), $value);)*
            fresh_context
    }) }
    macro_rules! eval { [[$to_eval:ident]] => ( $to_eval.try_into().unwrap()) }
    let fresh_context = make_context!{
        "or"  => closure!{fn or (l: bool, r: IntermediateValue) -> bool {
            l || eval![[r]]
        }},
        "and" => closure!{fn and(l: bool, r: IntermediateValue) -> bool {
            l && eval![[r]]
        }},
        "gte" => closure!{fn gte(l: i64, r: i64) -> bool {l >= r} },
        "lte" => closure!{fn lte(l: i64, r: i64) -> bool {l <= r} },
        "eq"  => closure!{fn eq (l: i64, r: i64) -> bool {l == r} },
        "gt"  => closure!{fn gt (l: i64, r: i64) -> bool {l > r} },
        "lt"  => closure!{fn lt (l: i64, r: i64) -> bool {l < r} },
        "add" => closure!{fn add(l: i64, r: i64) -> i64 {l + r} },
        "mul" => closure!{fn mul(l: i64, r: IntermediateValue) -> i64 {
            if l == 0 { 0 } else { l * (eval![[r]] : i64) }
        }},
        "div" => closure!{fn div(l: i64, r: i64) -> i64 {l / r} },
        "sub" => closure!{fn sub(l: i64, r: i64) -> i64 {l - r} },
        "if"  => closure!{fn if(cond: bool, then: IntermediateValue, else_: IntermediateValue) -> IntermediateValue {
            if cond { then } else { else_ }
        }},
        "bino" => closure!{fn bino(a: i64, b: i64, c: i64, x:i64) -> i64 {
            a * (x * x) + b * x + c
        }},
    };
    evaluate(expression, Rc::new(fresh_context))
        .and_then(|x| x.strict())
}
