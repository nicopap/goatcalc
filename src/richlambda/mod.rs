//! The λ-calculus completed with pattern-matching bindings, recusrive let
//! expressions, and `case` deconstructors
#[macro_use]
pub mod expression;
pub mod eval;
pub mod htmlify;
pub use self::expression::{Expression,Expression_,Pattern};

#[cfg(test)] mod test;
