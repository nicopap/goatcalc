use std::fmt;
use std::rc::Rc;
use std::cell::RefCell;

use fxhash::FxHashMap;

use super::{Expression,Pattern,Expression_};
use super::expression::ExPath;

macro_rules! context {
    ($($key:expr => $arrity:expr),*) => ({
        use $crate::richlambda::eval::Value::{Constructor,Structure,Evaluated};
        let mut fresh_context = Context::default();
        $(
            let constructor_value = if $arrity == 0 {
                Structure {
                    constructor: $key.into(),
                    values: Vec::new(),
                }
            } else {
                Constructor {
                    name: $key.into(),
                    arrity: $arrity,
                    stored_values: Vec::new(),
                }
            };
            fresh_context.insert($key.into(), constructor_value);
        )+
        fresh_context.insert("taint".into(), Evaluated(Taintedness::Dirty));
        fresh_context.insert("clean".into(), Evaluated(Taintedness::Clean));
        fresh_context.insert("mul".into(), Evaluated(Taintedness::UnkClean));
        fresh_context
})
}

impl<Annot> fmt::Debug for Value<Annot> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Value::*; match *self {
            Closure {ref context, ref body, ..} =>
                write!(f, "({})[{:?}]", body, context),
            ReClosure {ref context, ref body, ..} =>
                write!(f, "re({})[{:?}]", body, context),
            Evaluated(ref literal) => write!(f, "Val({:?})", literal),
            Structure { ref constructor, ref values } =>
                write!(f, "({} {:?})", constructor, values),
            Constructor { ref name, arrity, ref stored_values } =>
                write!(f, "({} {} {:?})", name, arrity, stored_values),
        }
    }
}
impl fmt::Display for Taintedness {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Taintedness::Clean => write!(f, "*c*"),
            Taintedness::Dirty => write!(f, "*d*"),
            Taintedness::UnkClean => write!(f, "*unk_c*"),
            Taintedness::UnkDirty => write!(f, "*unk_d*"),
        }
    }
}
impl<Annot> fmt::Display for Value<Annot> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Value::*; match *self {
            Evaluated(ref literal)   => write!(f, "{}", literal),
            Closure {ref body, ..}   => write!(f, "[[{}]]", body),
            ReClosure {ref body, ..} => write!(f, "[[{}]]", body),
            Structure {ref constructor, ref values} => {
                write!(f, "({}", constructor)?;
                for value in values.iter() {
                    write!(f, " {}", value)?;
                }
                write!(f, ")")
            },
            Constructor {ref name, arrity, ref stored_values} => {
                write!(f, "({}", name)?;
                for value in stored_values.iter() {
                    write!(f, " {}", value)?;
                }
                for _ in 1..((arrity as usize) - stored_values.len()) {
                    write!(f, " _")?;
                }
                write!(f, ")")
            },
        }
    }
}

impl<Annot> From<bool> for Value<Annot> {fn from(x: bool) -> Value<Annot> {Value::Evaluated(
    if x { Taintedness::Clean } else { Taintedness::Dirty }
)}}
impl<Annot> From<Taintedness> for Value<Annot> {fn from(x: Taintedness) -> Value<Annot> { Value::Evaluated(x) }}
impl From<bool> for Taintedness {fn from(x: bool) -> Taintedness {
    if x { Taintedness::Clean } else { Taintedness::Dirty }
}}

type RExpression<Annot> = Expression<String,Taintedness,Annot>;
type RPattern= Pattern<String,Taintedness>;

// Insert into the context a value deconstructed according to pattern
fn insert_pattern<Annot>(
    context: &mut Context<Annot>,
    pattern: RPattern,
    value: Value<Annot>
) -> Result<(),String> {
    use self::Pattern::*;
    match pattern {
        Literal(_) | Wildcard => Ok(()) ,
        Ident(name) => {
            context.insert(name, value);
            Ok(())
        },
        Constructor { name, arguments } => {
            match value {
                Value::Structure{constructor, values} => {
                    if constructor == name {
                        for (sub_pattern, sub_value)
                        in arguments.into_iter().zip(values.into_iter()) {
                            insert_pattern(context, sub_pattern, sub_value)?
                        }
                        Ok(())
                    } else {
                        Err(format!("pattern constructor {} did not match with \
                                    value constructor {}", name, constructor))
                    }
                },
                Value::Evaluated(taintedness) => {
                    if taintedness == Taintedness::UnkDirty
                      || taintedness == Taintedness::UnkClean
                    {
                        for sub_pattern in arguments {
                            insert_pattern(context, sub_pattern,
                                           Value::Evaluated(taintedness))?
                        }
                        Ok(())
                    } else {
                        Err(format!("pattern constructor {} did not match with \
                                    value {}", name, taintedness))
                    }
                },
                _ => {
                    Err(format!("pattern constructor {} did not match with \
                                value {}", name, value))
                },
            }
        },
    }
}

/// Evaluates a λ-expression using the given context.
fn evaluate<PreFn, Annot>(
    preproc: &PreFn,
    expression: RExpression<Annot>,
    context: Rc<Context<Annot>>
) -> Result<Value<Annot>,String>
    where PreFn: Fn(Rc<Context<Annot>>, Annot, Value<Annot>) -> Value<Annot>,
          Annot: Clone
{
    use self::Expression_::*;
    use self::Value::{Closure,ReClosure};
    let value = match expression.1 {
        Lambda { binding, box body } =>
            Ok(Closure { context: context.clone(), binding, body }),
        Application {box function, box argument} => {
            let function_value = evaluate(preproc, function, context.clone())?;
            let argument_value = evaluate(preproc, argument, context.clone())?;
            function_value.apply(preproc, argument_value)
        },
        Ident(ref name) =>
            match context.get(name) {
                None => Err(format!("attempt to access innexistant free \
                                    variable {}", name)),
                Some(val) => Ok((*val).clone()),
            },
        Literal(value) =>
            Ok(value.into()),
        Letrec { rec_name, binding, box body, box over } => {
            let mut new_context = Context::clone(&context);
            let evaluated = ReClosure {
                binding, body,
                context: context.clone(),
                rec_name: rec_name.clone()
            };
            new_context.insert(rec_name, evaluated);
            evaluate(preproc, over, Rc::new(new_context))
        },
        Let { binding, box value, box over } => {
            let mut new_context = Context::clone(&context);
            let evaluated = evaluate(preproc, value, context.clone())?;
            insert_pattern(&mut new_context, binding, evaluated)?;
            evaluate(preproc, over, Rc::new(new_context))
        },
        Case { box over, branches } => {
            let over_value = evaluate(preproc, over, context.clone())?;
            let mut matched_branches = match_branches(&over_value, branches);
            let evaluate_branch = |(pat,expr)| {
                let mut branch_context = Context::clone(&context);
                insert_pattern(&mut branch_context, pat, over_value.clone())?;
                evaluate(preproc, expr, Rc::new(branch_context))
            };
            let mut final_value = matched_branches.pop().ok_or(
                format!("no matching branches for case expression")
            ).and_then(evaluate_branch)?;

            for (pat,expr) in matched_branches {
                let evaluated = evaluate_branch((pat, expr))?;
                final_value = join(final_value, evaluated);
            }
            Ok(final_value)
        },
    }?;
    Ok((*preproc)(context, expression.0, value))
}

#[derive(Debug,Clone)]
pub struct Tracker {
    pub path: Vec<ExPath>,
    pub context: Rc<Context<Vec<ExPath>>>,
    pub value: Value<Vec<ExPath>>
}

/// Records the sequential execution of the given expression.
pub fn tracked_eval( annoted_expression: RExpression<Vec<ExPath>>)
    -> Result<(Value<Vec<ExPath>>,Vec<Tracker>),String>
{
    let fresh_context = context!(
        "Nil" => 0, "Cons" => 2, "Just" => 1, "Nothing" => 0
    );
    let tracker_cell : RefCell<Vec<Tracker>> = RefCell::new(vec![]);
    let track_evaluation = |
        context: Rc<Context<Vec<ExPath>>>,
        path: Vec<ExPath>,
        value: Value<Vec<ExPath>>
    | {
        let mut tracker = tracker_cell.borrow_mut();
        (*tracker).push(Tracker { path, context, value: value.clone() });
        value
    };
    let final_value = evaluate(
        &track_evaluation,
        annoted_expression,
        Rc::new(fresh_context)
    )?;
    let tracker_copy = Vec::clone(&tracker_cell.borrow());
    Ok((final_value, tracker_copy))
}

pub fn is_tainted<Annot>(value: &Value<Annot>) -> bool {
    use self::Value::*;
    match value {
        Evaluated(Taintedness::Dirty)
        | Evaluated(Taintedness::UnkDirty) => true,

        ReClosure{ref context, ref body, ..}
        | Closure{ref context, ref body, ..} =>
            context.iter()
                .filter(|(_,val)| is_tainted(val))
                .any(|(name, _)| body.appear_free(name)),
        Structure{ref values, ..} =>
            values.iter().any(|value| is_tainted(value)),

        _ => false,
    }
}

fn join<Annot:Clone>(rhs: Value<Annot>, lhs: Value<Annot>) -> Value<Annot> {
    use self::Value::Structure;
    fn equivalent<Annot>(rhs: &Value<Annot>, lhs: &Value<Annot>) -> bool {
        use self::Value::*;
        match (rhs, lhs) {
            (Evaluated(t1),
             Evaluated(t2))
                if t1 == t2 => true,
            (Closure { context:ref c1, binding:ref b1,.. },
             Closure { context:ref c2, binding:ref b2,.. })
                if c1.keys().eq(c2.keys()) && b1 == b2 => true,
            (ReClosure { context:ref c1, rec_name:ref n1, binding:ref b1,.. },
             ReClosure { context:ref c2, rec_name:ref n2, binding:ref b2,.. })
                if c1.keys().eq(c2.keys()) && b1 == b2 && n1 == n2 => true,
            (Structure { constructor:ref c1, values:ref vs1 },
             Structure { constructor:ref c2, values:ref vs2 }) =>
                c1 == c2 && vs1.iter().zip(vs2.iter())
                                .all(|(v1,v2)| equivalent(v1,v2)),
            _ => false,
        }
    }

    if rhs.is_struct() && lhs.is_struct() {
        if let
        (Structure{constructor: name1, values: values1},
         Structure{constructor: name2, values: values2}) = (rhs, lhs)
        {
            if name1 == name2 {
                let values = values1.into_iter().zip(values2.into_iter())
                    .map(|(val1, val2)| join(val1, val2))
                    .collect();
                Structure {constructor: name1, values}
            } else if values1.iter().any(is_tainted)
              || values2.iter().any(is_tainted)
            {
                Value::Evaluated(Taintedness::UnkDirty)
            } else {
                Value::Evaluated(Taintedness::UnkClean)
            }
        } else { unreachable!() }
    } else if equivalent(&rhs, &lhs) {
        rhs
    } else {
        if is_tainted(&rhs) || is_tainted(&lhs) {
            Value::Evaluated(Taintedness::UnkDirty)
        } else {
            Value::Evaluated(Taintedness::UnkClean)
        }
    }
}

#[derive(Clone,Copy,PartialEq)]
enum Match { Full, Partial, Diverges }
fn match_branches<Annot>(value: &Value<Annot>, branches: Vec<(RPattern, RExpression<Annot>)>)
    -> Vec<(RPattern, RExpression<Annot>)>
{
    use self::Match::*;
    fn match_order(rhs: Match, lhs: Match) -> Match {
        match (rhs, lhs) {
            (Diverges, _) => Diverges,
            (_, Diverges) => Diverges,
            (Partial, _) => Partial,
            (_, Partial) => Partial,
            (Full, Full) => Full,
        }
    }

    fn matchness<Annot>(pattern: &RPattern, value: &Value<Annot>) -> Match {
        use self::Value::{Evaluated,Structure};
        use self::Taintedness::*;
        use self::Pattern::*;
        match (pattern, value) {
            (Ident(_), _) | (Wildcard, _) => Match::Full,

            (Literal(_), Evaluated(Clean)) | (Literal(_), Evaluated(Dirty)) =>
                Match::Partial,

            (Literal(_), _) => Match::Diverges,

            (Constructor{ref name, ref arguments},
             Structure{ref constructor, ref values}) if name == constructor => {
                 arguments.iter().zip(values.iter())
                     .fold(Match::Full, |acc,(sub_pat, sub_val)| {
                         let sub_matchness = matchness(sub_pat, sub_val);
                         match_order(acc, sub_matchness)
                     })
            },
            (Constructor{..}, Structure{..}) =>
                Match::Diverges,

            (Constructor{..}, Evaluated(UnkClean))
            |(Constructor{..}, Evaluated(UnkDirty)) =>
                Match::Partial,

            (Constructor{..}, _) =>
                Match::Diverges,
        }
    }

    let mut ret = Vec::new();
    for (pattern, expr) in branches {
        match matchness(&pattern, &value) {
            Full => {
                ret.push((pattern,expr));
                break
            },
            Partial => {
                ret.push((pattern,expr));
            },
            Diverges => {},
        }
    }
    ret
}

type Context<Annot> = FxHashMap<String,Value<Annot>>;

#[derive(Clone, Debug, PartialEq, Copy)]
pub enum Taintedness { Clean, Dirty, UnkDirty, UnkClean }


#[derive(Clone)]
pub enum Value<Annot> {
    Evaluated(Taintedness),
    Closure {
        context: Rc<Context<Annot>>,
        binding: String,
        body: RExpression<Annot>,
    },
    ReClosure {
        context: Rc<Context<Annot>>,
        rec_name: String,
        binding: String,
        body: RExpression<Annot>,
    },
    Constructor {
        name: String,
        arrity: u8,
        stored_values: Vec<Value<Annot>>,
    },
    Structure {
        constructor: String,
        values: Vec<Value<Annot>>,
    },
}

impl<Annot:Clone> Value<Annot> {
    fn is_struct(&self) -> bool {
        if let Value::Structure{..} = self { true } else { false }
    }
    fn apply<PreFn>(
        self,
        preproc: &PreFn,
        argument: Value<Annot>,
    ) -> Result<Value<Annot>,String>
        where PreFn: Fn(Rc<Context<Annot>>, Annot, Value<Annot>) -> Value<Annot>
    {
        use self::Value::*;
        use self::Taintedness::*;
        match self {
            Closure { context, binding, body } => {
                let mut new_context = Context::clone(&context);
                new_context.insert(binding, argument);
                evaluate(preproc, body, Rc::new(new_context))
            },
            ReClosure { context, rec_name, binding, body } => {
                let mut new_context = Context::clone(&context);
                let rec_clos = ReClosure {
                    context: context,
                    rec_name: rec_name.clone(),
                    binding: binding.clone(),
                    body: body.clone()
                };
                new_context.insert(binding, argument);
                new_context.insert(rec_name, rec_clos);
                evaluate(preproc, body, Rc::new(new_context))
            },
            Constructor { name, arrity, mut stored_values } => {
                stored_values.push(argument);
                let value_count = stored_values.len() as u8;
                if value_count == arrity {
                    Ok(Structure {
                        constructor: name,
                        values: stored_values
                    })
                } else if value_count < arrity {
                    Ok(Constructor { name, arrity, stored_values })
                } else {
                    Err(format!("A constructor has more value stored than\
                                the structure it is building: {}", name))
                }
            },
            Evaluated(UnkDirty) => {
                Ok(Evaluated(UnkDirty))
            },
            Evaluated(UnkClean) if is_tainted(&argument) => {
                Ok( Evaluated(UnkDirty) )
            },
            Evaluated(UnkClean) => {
                Ok( Evaluated(UnkClean) )
            },
            anyelse =>
                Err(format!("attempt to apply {} to {} \
                            (which is not a function)", argument, anyelse)),
        }
    }
}


/// Evaluates a lambda expression and returns the string representation of
/// the resulting value.
pub fn interpret<Annot:Clone>(expression: Expression<String,Taintedness,Annot>) -> Result<Value<Annot>,String> {
    let fresh_context = context!(
        "Nil" => 0, "Cons" => 2, "Just" => 1, "Nothing" => 0
    );
    let preproc = Box::new(|_,_,v|v);
    evaluate(preproc.as_ref(), expression, Rc::new(fresh_context))
}
