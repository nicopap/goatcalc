//! The AST of the enriched λ-calculus, with helper types for generic marking.

use std::hash::{Hash,Hasher};
use std::fmt::{self,Display};

use ::lambda::Expression as simpleExpr;

#[macro_export]
macro_rules! richlambda {
    (λ $name:ident . $($body:tt)+) => (
        anch!($crate::richlambda::Expression_::Lambda {
            binding: stringify!($name).to_owned(),
            body: Box::new(richlambda!($($body)+))
        })
    );
    (letrec $rec_name:ident = λ $binding:ident . {$($body:tt)+} in $($over:tt)+) => (
        anch!($crate::richlambda::Expression_::Letrec {
            rec_name: stringify!($rec_name).to_owned(),
            binding: stringify!($binding).to_owned(),
            body: Box::new(richlambda!($($body)+)),
            over: Box::new(richlambda!($($over)+))
        })
    );
    (let ($($binding:tt)+) = {$($value:tt)+} in $($over:tt)+) => (
        anch!($crate::richlambda::Expression_::Let {
            binding: richlambda!(@pat $($binding)+),
            value: Box::new(richlambda!($($value)+)),
            over: Box::new(richlambda!($($over)+))
        })
    );
    (case ($($over:tt)+) of {$( ($($pat:tt)+) -> {$($expr:tt)+} ),+ }) => (
        anch!($crate::richlambda::Expression_::Case {
            over: Box::new(richlambda!($($over)+)),
            branches: vec![ $( (
                richlambda!(@pat $($pat)+),
                richlambda!($($expr)+)
            ) ),* ]
        })
    );
    (($($expr:tt)+)) => (
        richlambda!($($expr)+)
    );
    ($name:ident) => (
        anch!($crate::richlambda::Expression_::Ident(
                stringify!($name).to_owned()
        ))
    );
    (@pat _) => (
        $crate::richlambda::Pattern::Wildcard
    );
    (@pat ^ $constr:ident $( ($($arg:tt)+) )* ) => (
        $crate::richlambda::Pattern::Constructor {
            name: stringify!($constr).to_owned(),
            arguments: vec![ $( richlambda!(@pat $($arg)+) ),* ]
        }
    );
    (@pat $name:ident) => (
        $crate::richlambda::Pattern::Ident(stringify!($name).to_owned())
    );
    ($lval:tt $rval:tt $($rem:tt)+) => (
        richlambda!(($lval $rval) $($rem)+)
    );
    ($fun:tt  $arg:tt) => (
        anch!($crate::richlambda::Expression_::Application {
            function: Box::new(richlambda!($fun)),
            argument: Box::new(richlambda!($arg))
        })
    );
}

#[macro_export]
macro_rules! anch {
    ($expr:expr) => ( $crate::richlambda::expression::Anchored((),$expr))
}

/// an Anchored object is an `Object` marked with some `Attribute`
#[derive(Debug,Clone)]
pub struct Anchored<Object,Attribute>(pub Attribute,pub Object);

impl<Object:Hash, Attribute> Hash for Anchored<Object,Attribute> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.1.hash(state);
    }
}
impl<Object:Display, Attribute> Display for Anchored<Object,Attribute> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.1.fmt(f)
    }
}
impl<O:PartialEq, A> PartialEq for Anchored<O, A> {
    fn eq(&self, other: &Anchored<O,A>) -> bool {
        self.1 == other.1
    }
}
impl<O:PartialEq,A> Eq for Anchored<O, A> {}
impl<O,A> Anchored<O,A> {
    pub fn inner(&self) -> &O { &self.1 }
    pub fn annot(&self) -> &A { &self.0 }
    pub fn inner_own(self) -> O { self.1 }
    pub fn annot_own(self) -> A { self.0 }
}

/// Anchored expression
pub type Expression<Name,Lit,Annot> =
    Anchored<Expression_<Name,Lit,Annot>,Annot>;

/// An expression in the enriched λ-calculus
/// Generic over the literal `Lit` choosen by the implementer.
/// This very close to the expression defined in module `lambda`, with the
/// addition of the recursive let (`Letrec`) and the case expression (`Case`)
#[derive(Clone,Debug)]
pub enum Expression_<Name,Lit,Annot> {
    /// A λ-expression (`λx. Expr`)
    Lambda {
        /// The name of the binding (it is `x` in `λx. Expr`)
        binding: Name,
        /// The body (it is `Expr` in `λx. Expr`)
        body: Box<Expression<Name,Lit,Annot>>,
    },
    Application {
        /// `f` in `f x`
        function: Box<Expression<Name,Lit,Annot>>,
        /// `x` in `f x`
        argument: Box<Expression<Name,Lit,Annot>>,
    },
    /// A literal.
    Literal(Lit),
    /// A free or bound variable.
    Ident(Name),
    /// An expression where expressions can be bound to variables which
    /// are available in the expression itself.
    Letrec {
        rec_name: Name,
        binding: Name,
        body: Box<Expression<Name,Lit,Annot>>,
        over: Box<Expression<Name,Lit,Annot>>,
    },
    Let {
        binding: Pattern<Name,Lit>,
        value: Box<Expression<Name,Lit,Annot>>,
        over: Box<Expression<Name,Lit,Annot>>,
    },
    /// A deconstructing expression.
    Case {
        over: Box<Expression<Name,Lit,Annot>>,
        branches: Vec<(Pattern<Name,Lit>, Expression<Name,Lit,Annot>)>,
    }
}

/// A path through an AST of richlambda::Expression_
#[derive(Clone,Debug,Copy)]
pub enum ExPath {
    LBody,
    AppFun,
    AppArg,
    LetLet,
    LetIn,
    CaseOver,
    CaseBranch(u8),
}

/// Given a rich λ expression, marks each node with the `ExPath` pathing to
/// it.
pub fn name_branches<Name,Lit>(expression: Expression<Name,Lit,()>)
    -> Expression<Name,Lit,Vec<ExPath>>
{
    name_branches_helper(vec![], expression)
}


fn name_branches_helper<Name,Lit,Annot>(
    path: Vec<ExPath>,
    expression: Expression<Name,Lit,Annot>
) -> Expression<Name,Lit,Vec<ExPath>>
{
    use self::Expression_::*;
    use self::ExPath::*;
    use self::Anchored as A;
    macro_rules! labelwith {
        (box $path:expr, $to_label:expr) => ({
            let mut inner_path = path.clone();
            inner_path.push($path);
            Box::new( name_branches_helper(inner_path, $to_label) )
        });
        ($path:expr, $to_label:expr) => ({
            let mut inner_path = path.clone();
            inner_path.push($path);
            name_branches_helper(inner_path, $to_label)
        })
    }
    match expression.1 {
        Lambda { binding, box body } => {
            let body = labelwith!(box LBody, body);
            A(path, Lambda { binding, body })
        },
        Application { box function, box argument } => {
            let function = labelwith!(box AppFun, function);
            let argument = labelwith!(box AppArg, argument);
            A(path, Application { argument, function })
        },
        Literal(l) =>
            A(path,  Literal(l)),
        Ident(i) =>
            A(path,  Ident(i)),
        Let { binding, box value, box over } => {
            let value = labelwith!(box LetLet, value);
            let over = labelwith!(box LetIn, over);
            A(path, Let { over, value, binding })
        },
        Letrec { binding, rec_name, box body, box over } => {
            let body = labelwith!(box LetLet, body);
            let over = labelwith!(box LetIn, over);
            A(path, Letrec { over, body, binding, rec_name })
        },
        Case { box over, branches } => {
            let over = labelwith!(box CaseOver, over);
            let branches =
                branches.into_iter().enumerate().map(|(i, (pat, expr))|
                    (pat, labelwith!(CaseBranch(i as u8), expr))
                ).collect();
            A(path, Case { over, branches })
        },
    }

}

impl <Name:PartialEq,Lit,Annot>Expression<Name,Lit,Annot> {
    pub fn appear_free(&self, var: &Name) -> bool {
        use self::Expression_::*;
        match self.1 {
            Application {ref function, ref argument} =>
                function.appear_free(var) || argument.appear_free(var),

            //Never will be free, is bound :(
            Lambda{ref binding,..} if binding==var => false,
            Lambda{ref body, ..} /*otherwise*/     =>
                body.appear_free(var),
            Ident(ref name) if name == var    => true,
            Ident(_) | Literal(_) => false,
            Case{ref over, ref branches} =>
                over.appear_free(var)
                || branches.iter().any(|(_,expr)| expr.appear_free(var)),

            Let{ref over, ref value, ..} =>
                over.appear_free(var) || value.appear_free(var),
            Letrec{ref over, ref body, ..} =>
                over.appear_free(var) || body.appear_free(var),
        }
    }
}

/// `Pattern` describes deconstructing elements of the grammar.
#[derive(Clone,Debug)]
pub enum Pattern<Name,Lit> {
    Literal(Lit),
    Ident(Name),
    Constructor {
        name: Name,
        arguments: Vec<Pattern<Name,Lit>>,
    },
    Wildcard,
}

impl<Name,Lit> Pattern<Name,Lit> {
    pub fn into_ident(self) -> Option<Name> {
        if let Pattern::Ident(name) = self {
            Some(name)
        } else {
            None
        }
    }
}

impl<N:Display, L:Display> fmt::Display for Pattern<N,L> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Pattern::*;
        match *self {
            Literal(ref literal) =>
                write!(f, "{}", literal),
            Ident( ref name) =>
                write!(f, "{}", name),
            Constructor { ref name, ref arguments } => {
                write!(f, "{}", name)?;
                for arg in arguments.iter() {
                    match *arg {
                        Wildcard =>
                            write!(f," _")?,
                        Literal(_) | Ident(_) =>
                            write!(f," {}", arg)?,
                        Constructor {ref arguments, ..}
                                if !arguments.is_empty() =>
                            write!(f," ({})", arg)?,
                        Constructor {..} =>
                            write!(f," {}", arg)?,
                    };
                }
                Ok(())
            },
            Wildcard =>
                write!(f, "_"),
        }
    }
}

impl<N:Display, L:Display,A> fmt::Display for Expression<N,L,A> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Expression_::*;
        match self.1 {
            Lambda { ref binding, ref body } =>
                write!(f, "λ {}.{}", binding, body),

            Application { ref function, ref argument } => {
                match function.inner() {
                    Lambda {..} | Case {..} | Letrec {..} | Let {..} =>
                        write!(f, "({}) ", function),
                    Application {..} | Literal(_) | Ident(_) =>
                        write!(f, "{} ", function),
                }?;
                match argument.inner() {
                    Lambda{..} | Application{..}
                    | Case{..} | Letrec{..} | Let{..} =>
                        write!(f, "({})", argument),
                    Literal(_) | Ident(_) =>
                        write!(f, "{}", argument),
                }
            },
            Literal(ref literal) =>
                write!(f, "{}", literal),
            Ident( ref name) =>
                write!(f, "{}", name),
            Let { ref binding, ref value, ref over } =>
                write!(f, "let {} = {} in {}", binding, value, over),
            Letrec { ref binding, ref rec_name, ref body, ref over } => {
            write!(f, "letrec {} = λ {} . {} in {}", rec_name, binding,
                   body, over)
            },
            Case { ref over, ref branches } => {
                write!(f, "case {} of\n", over)?;
                for &(ref pattern, ref body) in branches.iter() {
                    write!(f, "    {} -> {}\n", pattern, body)?;
                }
                Ok(())
            },
        }
    }
}

impl<N,L> From<simpleExpr<N,L>> for Expression<N,L,()> {
    fn from(simple_lambda: simpleExpr<N,L>) -> Expression<N,L,()> {
        use self::simpleExpr as Slamb;
        use self::Expression_ as Rlamb;
        match simple_lambda {
            Slamb::Application { box function, box argument } => Anchored((),
                Rlamb::Application {
                    function: box function.into(),
                    argument: box argument.into(),
                }
            ),
            Slamb::Literal(literal) => Anchored((),
                Rlamb::Literal(literal)
            ),
            Slamb::Lambda { binding, box body } => Anchored((),
                Rlamb::Lambda {
                    binding,
                    body: box body.into(),
                }
            ),
            Slamb::Ident(ident) => Anchored((),
                Rlamb::Ident(ident)
            ),
        }
    }
}
