use std::char;
use std::fmt::Display;
use super::expression::{ExPath,Expression_,Expression,Anchored};

pub fn classify(path: &[ExPath]) -> String {
    use self::ExPath::*;
    path.iter().map(|path_element| match path_element {
        LBody=> 'b',
        AppFun=> 'f',
        AppArg=> 'a',
        LetLet=> 'l',
        LetIn=> 'i',
        CaseOver=> 'c',
        CaseBranch(which)=>
            char::from_digit(*which as u32, 36).unwrap()
                .to_ascii_uppercase(),
    }).collect()
}

pub fn to_html<N,L>(to_render: &Expression<N,L,Vec<ExPath>>) -> String
    where N: Display,
          L: Display,
{
    use self::Expression_::*;
    let &Anchored(ref path, ref expression) = to_render;
    match *expression {
        Lambda { ref binding, ref body } =>
            format!("<span id=\"{}\">λ{}.{}</span>",
                    classify(path), binding, to_html(body)),

        Application { ref function, ref argument } => {
            let fun_format = match function.inner() {
                Lambda {..} | Case {..} | Letrec {..} | Let {..} =>
                    format!("({})", to_html(function)),
                Literal(_)  | Ident(_)  | Application {..}       =>
                    format!("{}", to_html(function)),
            };
            let arg_format = match argument.inner() {
                Lambda{..} | Application{..}
                | Case{..} | Letrec{..} | Let{..} =>
                    format!("({})", to_html(argument)),
                Literal(_) | Ident(_) =>
                    format!("{}", to_html(argument)),
            };
            format!("<span id=\"{}\">{} {}</span>",
                    classify(path), fun_format, arg_format)
        },
        Literal(ref literal) =>
            format!("<span id=\"{}\">{}</span>", classify(path), literal),

        Ident(ref name) =>
            format!("<span id=\"{}\">{}</span>", classify(path), name),

        Let { ref binding, ref value, ref over } =>
            format!("<div id=\"{}\">let {} = <div>{}</div> \
                    in <div>{}</div></div>",
                    classify(path), binding, to_html(value), to_html(over)),

        Letrec { ref binding, ref rec_name, ref body, ref over } =>
            format!("<div id=\"{}\">letrec {} = λ {} . <div>{}</div> \
                    in <div>{}</div></div>", classify(path),
                    binding, rec_name, to_html(body), to_html(over)),

        Case { ref over, ref branches } => {
            let mut branches_format = String::new();
            for (ref pat, ref expr) in branches.iter() {
                branches_format +=
                    &format!("{} → <div>{}</div>", pat, to_html(expr));
            }
            format!("<div id=\"{}\">case {} of <div>{}</div></div>",
                    classify(path), to_html(over), branches_format)
        },
    }
}
