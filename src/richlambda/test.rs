use super::eval::interpret;

#[test]
fn test_constructor() {
    // Take the last element (tainted)
    let lambda_value = richlambda!(
        letrec map = λ f . { λ il .
            case (il) of {
                (^Nil) -> { Nil },
                (^Cons (h) (t)) -> {Cons (f h) (map f t)}
            }
        } in letrec tail = λ list . {
            case (list) of {
                (^Nil) -> {Nothing},
                (^Cons (t) (^Nil)) -> {Just t},
                (^Cons (_) (t)) -> {tail t}
            }
        } in let (ml) = { Cons clean (Cons clean (Cons taint Nil)) }
        in let (dl) = { map (mul clean)}
        in tail (dl ml)
    );
    let evaluated = interpret(lambda_value).unwrap();
    assert_eq!(format!("{}", evaluated), "(Just *unk_d*)");

    // Take former to last element (should be clean)
    let lambda_value = richlambda!(
        letrec map = λ f . { λ il .
            case (il) of {
                (^Nil) -> { Nil },
                (^Cons (h) (t)) -> {Cons (f h) (map f t)}
            }
        } in letrec tail = λ list . {
            case (list) of {
                (^Nil) -> {Nothing},
                (^Cons (t) (^Nil)) -> {Just t},
                (^Cons (_) (t)) -> {tail t}
            }
        } in letrec header = λ list . {
            case (list) of {
                (^Nil) -> {Nil},
                (^Cons (h) (^Cons (_) (^Nil))) -> {Cons h Nil},
                (^Cons (h) (t)) -> {Cons h (header t)}
            }
        } in let (ml) = { Cons clean (Cons clean (Cons taint Nil)) }
        in let (dl) = { map (mul clean)}
        in tail (header (dl ml))
    );
    let evaluated = interpret(lambda_value).unwrap();
    assert_eq!(format!("{}", evaluated), "(Just *unk_c*)")
}
