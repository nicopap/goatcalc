#[macro_use]
extern crate goatcalc;

use goatcalc::richlambda::*;

const HEADER_INLINE :  &'static str = r##"
<head>
    <meta charset="UTF-8">
    <title>Demo Verification</title>
    <style>body{font-family:monospace;font-size:20px}</style>
</head>
"##;

const JAVASCRIPT_INLINE_CODE : &'static str = r##"
var current_evaluation_line = 0;

function advance_execution(steps) {
  var new_line = current_evaluation_line + steps;
  if (new_line < 0 || new_line >= execution_sheet.length) { return }
  if (execution_sheet[new_line].block === "") { return }

  var old_state = execution_sheet[current_evaluation_line];
  document.getElementById(old_state.block).style.backgroundColor = "#ffffff00";

  current_evaluation_line = new_line;
  var new_state = execution_sheet[new_line];
  document.getElementById(new_state.block).style.backgroundColor = "#60f0f0";
  document.getElementById("showvalue").innerHTML = new_state.value;
}

document.addEventListener('keydown', function(event) {
  var pressed_key = event.key;
  if (pressed_key === "ArrowLeft" || pressed_key === "ArrowRight") {
    event.preventDefault();
    var steps = pressed_key == "ArrowRight" ? 1 : -1;
    advance_execution(steps)
  }
}, true);"##;

pub fn main() {
    let lambda_expr : Expression<String,eval::Taintedness,()> = richlambda!(
        letrec map = λ f . { λ il .
            case (il) of {
                (^Nil) -> { Nil },
                (^Cons (h) (t)) -> {Cons (f h) (map f t)}
            }
        } in letrec tail = λ list . {
            case (list) of {
                (^Nil) -> {Nothing},
                (^Cons (t) (^Nil)) -> {Just t},
                (^Cons (_) (t)) -> {tail t}
            }
        } in letrec header = λ list . {
            case (list) of {
                (^Nil) -> {Nil},
                (^Cons (h) (^Cons (_) (^Nil))) -> {Cons h Nil},
                (^Cons (h) (t)) -> {Cons h (header t)}
            }
        } in let (ml) = { Cons clean (Cons clean (Cons taint Nil)) }
        in let (dl) = { map (mul clean)}
        in tail (header (dl ml))
    );
    let marked_lambda = expression::name_branches(lambda_expr);
    let (_, execution_summary) =
        eval::tracked_eval(marked_lambda.clone()).unwrap();

    println!("{}", HEADER_INLINE);
    println!("<script>\nvar execution_sheet = [");
    for eval::Tracker { ref path, ref value, .. } in execution_summary.iter() {
        println!("   {{block: \"{}\", value: `{}`}},",
                 htmlify::classify(path), value)
    }
    println!("];{}</script>", JAVASCRIPT_INLINE_CODE);
    println!("<style>div{{margin-left:8px}}</style>");
    println!("<body>{}", htmlify::to_html(&marked_lambda));
    println!("<div id=\"showvalue\"></div></body>");
}
