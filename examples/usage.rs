#[macro_use] extern crate goatcalc;

pub fn main() {
    {
        let lambda_struct = lambda_expr!((λ f.λ y.λ x.f y x) (λ x.λ y.y) 3 4);
        let mut res = format!("{} -> ", lambda_struct);
        let result = goatcalc::interpret(lambda_struct).unwrap();
        res.push_str(format!("{}", result).as_ref());
        println!("{}", res);
    }

    {
        let lambda_struct = lambda_expr!((λ f.(λ x.f (x x)) (λ x.f (x x))) (λ fac.λ n.if (eq n 0) 1 (mul n (fac (sub n 1)))) 20);
        let mut res = format!("{} -> ", lambda_struct);
        let result = goatcalc::interpret(lambda_struct).unwrap();
        res.push_str(format!("{}", result).as_ref());
        println!("{}", res);
    }
}
