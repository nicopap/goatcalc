This library aims to provide an absolutely generic plugable λ-calculus
implementation and compiler for use for functional language implementation.

There is no pretence of performace/usability/whatever, take this as an exercise
in programming.

The implementation is guided by the 1987 edition of "The Implementation of
Functional Programming Languages" by Simon L. Peyton Jones.

